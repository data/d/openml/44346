# OpenML dataset: credit

https://www.openml.org/d/44346

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Subsampling of the dataset credit (44089) with

seed=0
args.nrows=2000
args.ncols=100
args.nclasses=10
args.no_stratify=True

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44346) of an [OpenML dataset](https://www.openml.org/d/44346). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44346/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44346/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44346/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

